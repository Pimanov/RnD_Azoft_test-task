from numpy import *
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.wrappers import TimeDistributed
from keras.layers.recurrent import LSTM

''' Сеть LSTM обучается на тексте последних 3х статей которые находятся в файле:
    article_texts.txt.

    Сеть получает на вход последовательноть из слов и знаков препинания
    (а также переносов строк) определенной длины (3 -- 5)
    и предсказывает следующее слово или знак препинания.
    Каждое уникальное слово или знак препинания в тексте получает номер,
    а затем унитарный код (one-hot vector). Унитарные коды, в свою очередь,
    участвуют в обучении и тестировании. Программа использует библиотеку keras
    https://keras.io/ '''


n = 3                                                # the deep for prediction
''' 'n' represents the number of consecutive words (or punctuation marks)
     that participate in the prediction of the next one '''
test_input = ['1. Мы разрабатываем и понимаем',
              '2. Сеть показала хорошие результаты',
              '3. На вход сети подается']            # test strings


'''     First step. Load end prepare the data
        We want to split the text to the single words and punctuation marks '''
papers = open('R&D article_texts.txt', 'r')
words_n_marks = []


def prepare_text(one_row, list_of_objects):
    """Add spaces for comfy splitting and split"""
    one_row = one_row.replace('. ', ' . ')
    one_row = one_row.replace('.\n', ' .\n')
    one_row = one_row.replace(', ', ' , ')
    one_row = one_row.replace(': ', ' : ')
    one_row = one_row.replace(':\n', ' :\n')
    one_row = one_row.replace('\n', ' \n')
    one_row = one_row.replace('(', '( ')
    one_row = one_row.replace(')', ' )')
    one_row = one_row.replace('~', '~ ')
    spl = one_row.split(' ')
    if '' in spl:
        spl.remove('')
    list_of_objects.extend(spl)


for row in papers:
    if len(row) > 10:               # it's not obligatory
        prepare_text(row, words_n_marks)

papers.close()

print(' The total number of objects (words and punctuation marks):', len(words_n_marks))
print('The number of unique objects:', len(set(words_n_marks)))

d_len = len(set(words_n_marks))     # number of unique objects
m_len = len(words_n_marks)          # total number of objects

'''   The second. Enumerate the unique objects and create the list of object's numbers  '''
my_dictionary = dict(zip(set(words_n_marks), range(len(set(words_n_marks)))))
key_list = [my_dictionary[i] for i in words_n_marks]


def to_vec(i):
    """ Create the one-hot vector from the
     number (or index) of object """
    vec = zeros(d_len, dtype=int)
    vec[i] = 1
    return vec


def find_key(input_dict, value):
    """ Find the key of string object """
    for c_key, c_value in input_dict.items():
        if c_value == value:
            return c_key


''' The third step. Create the training data as 'n' consecutive objects
    and next one for the right answer  '''
X_train = []
y_train = []
for i in range(len(key_list) - n):
    X_train.append([to_vec(key_list[i + j]) for j in range(n)])
    y_train.append(to_vec(key_list[i + n]))

X_train = asarray(X_train)
y_train = asarray(y_train)


''' The fourth step. Create the model using keras package.
    It is something like Doc2vec, but with taking into account the order of words in the text
    The model consist of one time distributed array of dense layers with 150 units,
    one LSTM recurrent layer with 150 units, one dropout to avoid overfitting (more or less)
    and the 'softmax' activation to get the output object.  '''
model = Sequential()
model.add(TimeDistributed(Dense(150), input_shape=(n, d_len, )))
model.add(LSTM(150))
model.add(Dropout(0.2))
model.add(Dense(d_len))
model.add(Activation('softmax'))
model.compile(loss='categorical_crossentropy', metrics=['categorical_accuracy'],
              optimizer='adam')

''' The fifth step. Fit the model.  '''
model.fit(X_train, y_train, epochs=12, batch_size=16)

''' The last step. Test the model.  '''

m = 25                              # the number of words to be predicted

for test_string in test_input:
    test_words_n_marks = []
    output_string = test_string
    prepare_text(test_string, test_words_n_marks)

    test_key_list = [my_dictionary[i] for i in test_words_n_marks]
    y = [to_vec(test_key) for test_key in test_key_list]
    for i in range(m):
        [res] = model.predict(asarray([y[-n:]]))
        v = argmax(res)                 # the index of answer
        y.append(to_vec(v))
        output_string += ' '
        output_string += find_key(my_dictionary, v)

    ''' show the result as the processed string '''
    output_string = output_string.replace(' .', '.')
    output_string = output_string.replace(' ,', ',')
    output_string = output_string.replace(' :', ':')
    output_string = output_string.replace(' \n', '\n')
    output_string = output_string.replace('( ', '(')
    output_string = output_string.replace(' )', ')')

    print(output_string)
